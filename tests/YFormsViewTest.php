<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 31/10/13
 * Time: 14:03 
 */
set_include_path(__DIR__ . '/../library');
require_once 'YF/Forms.php';
require_once 'YF/FormsElements.php';
require_once 'YF/FormsElements/Text.php';
require_once 'YF/YFormsView.php';

/**
 * Class YFormsViewTest
 */
class YFormsViewTest extends PHPUnit_Framework_TestCase {
    /**
     * @var \YF\Forms
     */
    protected $fixture;
    protected $formName = 'test';

    protected function setUp()
    {
        $this->fixture = new YF\Forms($this->formName);

    }

    public function testBegin()
    {
        $return = YFormsView::begin($this->fixture);
        $this->assertEquals('<form id="' . $this->fixture->getName() . '" class="">', $return);
    }

    public function testEnd()
    {
        $return = YFormsView::end();
        $this->assertEquals('</form>', $return);
    }

    public function testTextElement()
    {
        $elementName = 'element_name';
        $element = new YF\FormsElements_Text($elementName);
        $this->fixture->addElement($element);
        $class = 'className';

        $return = YFormsView::element('text', $elementName, $this->fixture, $class);

        $equals = '<input type="text" id="' . $elementName . '-' . $this->fixture->getName() . '" class="' . $class. '" name="' . $elementName . '" value="">';

        $this->assertEquals($equals, $return);
    }

    public function testTextWithValueElement()
    {
        $elementName = 'element_name';
        $element = new YF\FormsElements_Text($elementName);
        $value = 'test_value';
        $element->setValue($value);
        $this->fixture->addElement($element);
        $class = 'className';

        $return = YFormsView::element('text', $elementName, $this->fixture, $class);

        $equals = '<input type="text" id="' . $elementName . '-' . $this->fixture->getName() . '" class="' . $class. '" name="' . $elementName . '" value="' . $value . '">';

        $this->assertEquals($equals, $return);
    }

    protected function tearDown()
    {
        $this->fixture = NULL;
    }
}
 