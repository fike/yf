<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 31/10/13
 * Time: 11:43 
 */
set_include_path(__DIR__ . '/../library');
require_once 'YF/Forms.php';
require_once 'YF/FormsElements.php';
require_once 'YF/FormsElements/Text.php';

/**
 * Class YFormsTest
 */
class YFormsTest extends PHPUnit_Framework_TestCase {

    /** @var \YF\Forms */
    protected $fixture;
    protected $formName = 'test';

    protected function setUp()
    {
        $this->fixture = new \YF\Forms($this->formName);
    }

    public function testSetName()
    {
        $this->assertEquals($this->formName . 'Form', $this->fixture->getName());
    }

    public function testSetAction()
    {
        $formAction = '/test/test';
        $this->fixture->setAction($formAction);
        $params = $this->fixture->getParams();
        $this->assertEquals($formAction, $params['action']);
    }

    public function testSetMethod()
    {
        $this->fixture->setMethod('some_value');
        $params = $this->fixture->getParams();
        $this->assertEquals('post', $params['method']);

        $this->fixture->setMethod('get');
        $params = $this->fixture->getParams();
        $this->assertEquals('get', $params['method']);

        $this->fixture->setMethod('post');
        $params = $this->fixture->getParams();
        $this->assertEquals('post', $params['method']);
    }

    public function testAddElement()
    {
        $elementName = 'testElement';
        $formElement = new YF\FormsElements_Text($elementName);

        $this->fixture->addElement($formElement);
        $this->assertTrue($this->fixture->isElement($elementName));
    }

    protected function tearDown()
    {
        $this->fixture = NULL;
    }

}
 