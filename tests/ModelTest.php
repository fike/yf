<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 31/10/13
 * Time: 14:37 
 */
set_include_path(__DIR__ . '/../library' . PATH_SEPARATOR . __DIR__ . '/../../library/zf1/library');
require_once 'YF/Model.php';
require_once 'Zend/Registry.php';
require_once 'Zend/Db/Adapter/Pdo/Mysql.php';

/**
 * Class ModelTest
 */
class ModelTest extends PHPUnit_Framework_TestCase {
    /**
     * @var \YF\Model
     */
    protected $fixture;
    protected $tableName = 'test';
    protected $dbAdapter;

    protected $dbConfig = array(
        'host'     => '10.0.1.219',
        'username' => 'root',
        'password' => 'qwerty',
        'dbname'   => 'model_test'
    );

    protected function setUp()
    {
        $dbAdapter = new Zend_Db_Adapter_Pdo_Mysql($this->dbConfig);
        Zend_Registry::set('dbAdapter', $dbAdapter);
        $this->fixture = new YF\Model($this->tableName);
    }

    public function testInit()
    {
        $this->assertInstanceOf('\YF\Model', $this->fixture);
        $this->assertEquals('test', $this->fixture->getTableName());
    }

    public function testFetchAll()
    {
        $this->assertGreaterThan(0, count($this->fixture->fetchAll()));
    }

    public function testFetchRow()
    {
        $this->assertTrue(is_array($this->fixture->fetchRow()));
        $this->assertTrue(is_array($this->fixture->fetchRow(1)));
        $this->assertFalse(is_array($this->fixture->fetchRow(2)));
        $this->assertTrue(is_array($this->fixture->fetchRow('id = 1')));
        $this->assertTrue(is_array($this->fixture->fetchRow(array('id = 1', 'title !=\'\''))));
        $this->assertFalse(is_array($this->fixture->fetchRow(array('id = 1', 'title =\'not_exists_value\''))));
    }

    public function testFetchPairs()
    {
        $this->assertGreaterThan(0, $this->fixture->fetchPairs('', array('title', 'is_del')));
    }

    public function testRelation()
    {
        $userModel = new YF\Model('users');
        $this->fixture->addRelation('user', 'user_id', $userModel);

        $result = $this->fixture->fetchAll();
        $this->assertGreaterThan(0, count($result[0]['user']));

        $result = $this->fixture->fetchRow(1);
        $this->assertGreaterThan(0, count($result['user']));
    }

    public function testSave()
    {
        // insert new
        $id = $this->fixture->save(array('title' => 'new record', 'no_field' => 'qwe'));
        $data = $this->fixture->fetchRow($id);
        $this->assertEquals($id, $data['id']);
        $this->assertGreaterThan(0, $data['created_date']);

        //update
        $this->fixture->save(array('id' => 1, 'title' => 'update_record', 'no_field' => 'qwe'));
        $data = $this->fixture->fetchRow(1);
        $this->assertEquals('update_record', $data['title']);
        $this->assertGreaterThan(0, $data['modified_date']);

        //insert with user_id
        $id = $this->fixture->setUser(1)->save(array('title' => 'test user_id'));
        $data = $this->fixture->fetchRow($id);
        $this->assertEquals($id, $data['id']);
        $this->assertEquals(1, $data['user_id']);


        //update with user_id
        $this->fixture->setUser(1)->save(array('id' => 1, 'title' => 'new record with user', 'no_field' => 'qwe'));
        $data = $this->fixture->fetchRow(1);
        $this->assertEquals('new record with user', $data['title']);
        $this->assertEquals(1, $data['modified_user_id']);

        //update with where
        $this->fixture->save(array('id' => 1, 'title' => 'only user_id = 1'), 'user_id = 1');
        $data = $this->fixture->fetchRow(1);
        $this->assertEquals('only user_id = 1', $data['title']);
        $data = $this->fixture->fetchAll('id != 1 AND title = \'only user_id = 1\'');
        $this->assertEquals(0, count($data));
    }

    protected function tearDown()
    {
        $this->fixture = NULL;
    }

}
 