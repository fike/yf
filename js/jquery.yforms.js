/**
 * Created by fayko on 28/10/13.
 */
(function( $ ){
    $.fn.yforms = function(options) {
        var settings = $.extend({
            reload: false
        }, options);

        return this.filter('form').each(function() {

            $(this).append('<input type="hidden" name="ajaxForm" value="1">');

            $(this).ajaxForm({
                beforeSubmit: function(formData, $form){
                    $('.error', $form).html('').slideUp('fast');
                },
                success: function(data, statusText, xhr, $form){
                    if(typeof data.errors != 'undefined') {
                        $.each(data.errors, function(key){
                            $('.' + key + 'Error', $form).html(this).slideDown('fast');
                        });
                    } else {

                        if(settings.reload) {
                            document.location.reload();
                        }

                        if(typeof settings.success == 'function') {
                            settings.success.call(this, $form);
                        }

                    }
                }
            });
        });

    }
}(jQuery));