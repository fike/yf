<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 01/11/13
 * Time: 15:56 
 */

namespace YF;

/**
 * Class Upload
 * @package YF
 */
class Upload {

    /**
     * @param string $dir
     */
    private static function makeDir($dir)
    {
        if(!is_dir($dir)) mkdir($dir, 0755, true);
    }

    /**
     * @param string $filename
     * @return string
     */
    private static function generateFilename($filename)
    {
        $ext = mb_strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        $filename = md5(microtime()) . '.' . $ext;
        return $filename;
    }

    /**
     * @param $file
     * @param $destination
     * @param $filename
     * @param array $params
     */
    private static function _uploadImage($file, $destination, $filename, $params = array())
    {
        if(!$params) {
            $params[] = array('type' => Model::IMAGE_ORIGINAL);
        }

        foreach($params as $imageSize) {

            switch($imageSize['type']) {
                case Model::IMAGE_CROP:
                    $th = new \imagick();
                    $th->readImage($file);
                    $th->cropThumbnailImage($imageSize['width'], $imageSize['height']);
                    $th->sharpenImage(1,3);
                    $th->setImageCompression(\imagick::COMPRESSION_JPEG);
                    $th->setImageCompressionQuality(85);
                    $th->stripImage();
                    $th->writeImage($destination . $imageSize['width'] . '_' . $imageSize['height'] . '_' . $filename);
                    break;
                case Model::IMAGE_RESIZE:
                    $th = new \imagick();
                    $th->readImage($file);
                    $th->resizeImage($imageSize['width'], $imageSize['height'], \imagick::FILTER_LANCZOS, 1);
                    $th->sharpenImage(1,3);
                    $th->setImageCompression(\imagick::COMPRESSION_JPEG);
                    $th->setImageCompressionQuality(85);
                    $th->stripImage();
                    $th->writeImage($destination . $imageSize['width'] . '_' . $imageSize['height'] . '_' . $filename);
                    break;
                case Model::IMAGE_ORIGINAL:
                    move_uploaded_file($file, $destination . $filename);
                    break;
            }

        }
    }

    /**
     * Загружает картинку и возвращает сгенерированное имя, так же при необходимости изменяет размеры
     * @param $name
     * @param $dir
     * @param array $params
     * @throws \Exception
     * @return bool|string
     */
    public static function uploadImage($name, $dir, $params = array())
    {
        if(!isset($_FILES[$name]['name']) or $_FILES[$name]['tmp_name'] == '') {
            return false;
        }

        self::makeDir($dir);

        $filename = self::generateFilename($_FILES[$name]['name']);

        $size = getimagesize($_FILES[$name]['tmp_name']);

        if(!$size) {
            throw new \Exception('Ошибка определения размера картинки');
        }

        self::_uploadImage($_FILES[$name]['tmp_name'], $dir, $filename, $params);

        return $filename;
    }

    /**
     * @param string $name
     * @param string $dir
     * @param array $params
     * @return array
     */
    public static function uploadImages($name, $dir, $params = array())
    {
        self::makeDir($dir);
        $index = 0;
        if(!$params) {
            $params[] = array('type' => Model::IMAGE_ORIGINAL);
        }

        $return = array();

        foreach($_FILES[$name]['name'] as $filename) {

            $filename = self::generateFilename($filename);

            $size = getimagesize($_FILES[$name]['tmp_name'][$index]);

            if(!$size) {
                continue;
            }

            self::_uploadImage($_FILES[$name]['tmp_name'][$index], $dir, $filename, $params);

            $return[] = $filename;

            $index++;
        }

        return $return;
    }

} 