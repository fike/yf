<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_File
 * @package YF
 */
class FormsElements_File extends FormsElements
{
    /**
     * Устанавливает изначение из переменой $_FILES
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->_element['value'] = $_FILES[$this->getName()]['tmp_name'];
        return $this;
    }

    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);

        return '<input type="file" id="' . $id . '" class="' . $class . '" name="' . $this->getName() . '">';
    }

} 