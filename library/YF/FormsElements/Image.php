<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_File
 * @package YF
 */
class FormsElements_Image extends FormsElements
{
    /**
     * Устанавливает изначение из переменой $_FILES
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->_element['value'] = $_FILES[$this->getName()]['tmp_name'];
        return $this;
    }

    /**
     * @param $id
     * @param Model $model
     */
    public function save($id, Model $model)
    {
        $filename = Upload::uploadImage($this->getName(), $model->getImageSavePath(), $model->getImageParams($this->getName()));

        if($filename) {
            $model->update(array($this->getName() => $filename), $id);
        }
    }

    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);

        return '<input type="file" id="' . $id . '" class="' . $class . '" name="' . $this->getName() . '">';
    }

} 