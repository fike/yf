<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_Select
 * @package YF
 */
class FormsElements_Select extends FormsElements
{

    /**
     * Устанавливает список возможных значений
     * @param array $values
     * @return $this
     */
    public function setValues($values = array())
    {
        $this->_element['values'] = $values;
        return $this;
    }

    /**
     * Возвращает список значений
     * @return mixed
     */
    public function getValues()
    {
        return $this->_element['values'];
    }

    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);

        $option = '';
        $value = $this->getValue();
        foreach ($this->getValues() as $optValue => $optTitle) {
            if($optValue == $value) {
                $selected = ' selected="selected"';
            } else {
                $selected = '';
            }
            $option .= '<option value="' . $optValue . '"' . $selected . '>' . $optTitle . '</option>';
        }

        $return = '<select id="' . $id . '" class="' . $class . '" name="' . $this->getName() . '">' . $option . '</select>';

        return $return;
    }
}