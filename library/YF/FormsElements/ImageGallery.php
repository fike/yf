<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_File
 * @package YF
 */
class FormsElements_ImageGallery extends FormsElements
{
    /**
     * Устанавливает изначение из переменой $_FILES
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->_element['value'] = $_FILES[$this->getName()];
        return $this;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        $values = $this->getValue();
        $valid = true;

        foreach($values['tmp_name'] as $index => $value) {

            if($value == '') {
                $this->unsetNotValidFile($index);
                continue;
            }

            if(!\Zend_Validate::is($value, 'File_isImage')) {
                $this->unsetNotValidFile($index);
            }

        }

        return $valid;
    }

    /**
     * Устанавливает модель для сохранения галерии в базе
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->_element['model'] = $model;
        return $this;
    }

    /**
     * Сохраняет галлерею
     * @param $parentId
     */
    public function save($parentId)
    {
        /** @var Model $model */
        $model = $this->_element['model'];

        $files = Upload::uploadImages($this->getName(), $model->getImageSavePath(), $model->getImageParams($this->getName()));
        foreach($files as $file) {
            $model->save(array('file' => $file, 'parent_id' => $parentId));
        }

    }

    /**
     * Удаляет файл из массива значений
     * @param int $index
     */
    private function unsetNotValidFile($index)
    {
        $filesItems = array('name', 'type', 'tmp_name', 'error', 'size');
        foreach($filesItems as $item) {
            unset($this->_element['value'][$item][$index]);
        }

    }

    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);

        return '<input type="file" id="' . $id . '" class="' . $class . '" multiple="multiple" name="' . $this->getName() . '[]">';
    }

} 