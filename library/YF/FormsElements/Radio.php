<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_Radio
 * @package YF
 */
class FormsElements_Radio extends FormsElements
{

    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);

        return '<input type="radio" id="' . $id . '" class="' . $class . '" name="' . $this->getName() . '" value="' . $this->getValue() . '">';
    }

} 