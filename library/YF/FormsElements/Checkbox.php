<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_Checkbox
 * @package YF
 */
class FormsElements_Checkbox extends FormsElements
{
    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);


        $checked = '';
        if((bool)$this->getValue()) {
            $checked = ' checked="checked"';
        }
        $return = '<input type="hidden" name="' . $this->getName() . '" value="0">';
        $return .= '<input type="checkbox" id="' . $id . '" class="' . $class . '" ' . $checked . ' name="' . $this->getName() . '" value="1">';

        return $return;
    }
} 