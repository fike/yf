<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_Text
 * @package YF
 */
class FormsElements_Text extends FormsElements
{

    /**
     * Устанавливает плейсхолдер
     * @param string $label
     * @return $this
     */
    public function setPlaceholder($label)
    {
        $this->_element['placeholder'] = $label;
        return $this;
    }

    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);
        $placeholder = '';
        if(isset($this->_element['placeholder'])) {
            $placeholder = ' placeholder="' . $this->_element['placeholder'] . '"';
        }

        return '<input type="text" id="' . $id . '" class="' . $class . '" name="' . $this->getName() . '" value="' . $this->getValue() . '"' . $placeholder . '>';
    }

} 