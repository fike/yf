<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_Hidden
 * @package YF
 */
class FormsElements_Hidden extends FormsElements
{
    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        return '<input type="hidden" id="' . $id . '" name="' . $this->getName() . '" value="' . $this->getValue() . '">';
    }
}