<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:35
 */
namespace YF;
/**
 * Class FormsElements_Submit
 * @package YF
 */
class FormsElements_Submit extends FormsElements
{
    /**
     * Возвращает HTML-код элемента
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public function getHtml(Forms $form, $class = '')
    {
        $id = $this->getElementId($form);
        $class = $this->getElementClass($class);

        return '<input type="submit" id="' . $id . '" class="' . $class . '"  value="' . $this->getName() . '">';
    }

} 