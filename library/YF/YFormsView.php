<?php
/**
 * Class Forms
 * Класс вывода форм  в шаблон
 */
use YF\Forms;
/**
 * Class YFormsView
 */
class YFormsView
{

    /**
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public static function begin(Forms $form, $class="")
    {
        $params = self::params($form);
        return '<form' . $params . ' class="' . $class . '">';
    }

    /**
     * @return string
     */
    public static function end()
    {
        return '</form>';
    }

    /**
     * Возвращает элемент для вывода его в шаблон
     * @param $type - тип элемента
     * @param $title - имя элемена
     * @param Forms $form - массив с описанием формы
     * @param string $class - классы
     * @return string
     */
    public static function element($type, $title, $form = null, $class = '')
    {
        // Если форма не установленна, значит делаем заглушку для верстки
        if(is_null($form)) {
            $form = new Forms('test' . rand(0, 999));
            $elementClass = 'YF\FormsElements_' . ucfirst($type);
            $element = new $elementClass($title);
            $form->addElement($element);
        } else {
            $element = $form->getElement($title);
        }

        $return = $element->getHtml($form, $class);

        return $return;
    }

    /**
     * Алиас для Forms::element
     * @param $title
     * @param Forms $form
     * @param string $class
     * @param string $id
     * @return string
     */
    public static function text($title, $form = null, $class = '', $id = '')
    {
        return self::element('text', $title, $form, $class, $id);
    }

    /**
     * Алиас для Forms::element
     * @param $title
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public static function textarea($title, $form = null, $class = '')
    {
        return self::element('textarea', $title, $form, $class);
    }

    /**
     * Алиас для Forms::element
     * @param $title
     * @param Forms $form
     * @param string $class
     * @return string
     */
    public static function submit($title, $form = null, $class = '')
    {
        return self::element('submit', $title, $form, $class);
    }

    /**
     * Алиас для Forms::element
     * @param $title
     * @param Forms $form
     * @return string
     */
    public static function hidden($title, Forms $form)
    {
        return self::element('hidden', $title, $form, '');
    }


    /**
     * Возвращает лейбл элемента для вывода его в шаблон
     * @param $elementName - имя элемента
     * @param $label - лейбл
     * @param Forms $form - объект формы
     * @return string
     */
    public static function label($elementName, $form = null, $label = '')
    {
        if(!is_null($form) and $label == '') {
            $label = $form->getElement($elementName)->getLabel();
        }
        if(is_null($form)) {
            $formName = 'testForm';
        } else {
            $formName = $form->getName();
        }

        return '<label for="' . $elementName . '-' . $formName .'">'. $label . '</label>';
    }

    /**
     * Возвращает сообщение об ошибке значения элемента для вывода его в шаблон
     * @param $elementName - имя элемента
     * @param Forms $form - массив с описанием формы
     * @return string
     */
    public static function error($elementName, $form = null)
    {

        $errorMessage = $form->getElement($elementName)->getErrors();
        if($errorMessage == '') {
            return '<div class="error ' . $elementName . 'Error"></div>';
        } else {
            if(is_array($errorMessage)) {
                $return = '';
                foreach($errorMessage as $error) {
                    $return .= $error;
                    break;
                }
                return '<div class="error ' . $elementName . 'Error" style="display: block;">' . $return . '</div>';
            } else {
                return '<div class="error ' . $elementName . 'Error" style="display: block;">' . $errorMessage . '</div>';
            }

        }

    }


    /**
     * Возвращает параметры формы, такие action, method и тд для вывода в шаблон
     * @param $form
     * @return string
     */
    public static function params(Forms $form)
    {
        $return = '';

        $params = $form->getParams();

        foreach($params as $key => $value) {
            $return .= ' ' . $key . '="' . $value . '"';
        }
        return $return;
    }

}