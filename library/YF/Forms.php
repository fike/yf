<?php
/**
 * Class YForms
 * Конструктор html-форм
 */
namespace YF;
/**
 * Class Forms
 * @package YF
 */
class Forms
{

    /**
     * @var array Параметры формы
     */
    private $_formParams = array();

    /**
     * @var array Элементы формы
     */
    private $_formElements = array();

    /**
     * @var bool Ответ должен быть в json
     */
    private $_ajaxForm = false;

    /**
     * @var Model
     */
    private $_model;

    /**
     * @param $name - имя формы
     */
    public function __construct($name)
    {
        $this->setName($name);
        return $this;
    }

    /**
     * Устанавливает имя (id) формы
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->_formParams['id'] = $name . 'Form';
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_formParams['id'];
    }


    /**
     * Устанавливает action формы
     * @param $action
     * @return $this
     */
    public function setAction($action)
    {
        $this->_formParams['action'] = $action;
        return $this;
    }

    /**
     * Устнавливает метод отправки формы
     * @param $method
     * @return $this
     */
    public function setMethod($method)
    {
        if($method == 'get') {
            $method = 'get';
        } else {
            $method = 'post';
        }

        $this->_formParams['method'] = $method;
        return $this;
    }

    /**
     * Тип отправки формы (ajax)
     * @param $type
     * @return $this
     */
    public function setSendType($type)
    {
        if($type == 'ajax') {
            $this->_ajaxForm = true;
        }

        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setAttribute($key, $value)
    {
        $this->_formParams[$key] = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAjaxForm()
    {
        return $this->_ajaxForm;
    }

    /**
     * Добавляет элемент формы, тип элемена устанавливается в шаблоне при вызове необходимого метода,
     * например: YForms::text('title', $form, 'b-input')
     * @param FormsElements|string $element - имя элемента
     * @param string $name
     * @param array $params
     * @internal param array $validators - массив валидаторов с именами Zend_Validators
     * @internal param bool $required - обязательный элемент
     * @internal param array $filters - массив фильтров с именами Zend_Filters
     * @return $this
     */
    public function addElement($element, $name = '', $params = array())
    {
        if(!$element instanceof FormsElements) {
            $elementClass = 'FormsElements_' . ucfirst($element);
            $element = new $elementClass($name);


            foreach($params as $method => $param) {
                if(method_exists($element, 'set' . ucfirst($method))) {
                    $element->{'set' . ucfirst($method)}($param);
                } else if(method_exists($element, 'add' . ucfirst($method))){
                    $element->{'add' . ucfirst($method)}($param);
                }
            }

        }
        $this->_formElements[$element->getName()] = $element;
        return $this;
    }

    /**
     * Устанавливает значия для элементов формы
     * @param array $values
     * @return $this
     */
    public function setValues($values = array())
    {
        foreach($values as $key => $value) {
            if(!$this->isElement($key)) {
                continue;
            }

            $this->getElement($key)->setValue($value);
            if($key == 'ajaxForm') {
                $this->setSendType('ajax');
            }
        }
        return $this;
    }

    /**
     * Валидация формы, устанавливает значения формы, фильтрует значения и проверяет значения,
     * на соответствие установленным валидаторам
     * @param array $values
     * @return bool
     */
    public function isValid($values = array())
    {
        $this->setValues($values);

        $valid = true;
        /** @var $element FormsElements */
        foreach ($this->_formElements as &$element) {

            $element->filter();

            $value = $element->getValue();

            if($element->isRequired() and empty($value)) {
                $element->setErrors($element->getRequiredMessage());
                $valid = false;
            } else {
                if(!$element->isValid()) {
                    $valid = false;
                }
            }
        }

        return $valid;

    }

    /**
     * Возвращает все значения формы
     * @return array
     */
    public function getValues()
    {
        $values = array();
        /** @var FormsElements $element */
        foreach ($this->_formElements as $key => $element) {
            if(!$element instanceof FormsElements_File) {
                $values[$key] = $element->getValue();
            }
        }

        return $values;
    }

    /**
     * Возвращает все ошибки формы
     * @return array
     */
    public function getErrors()
    {
        $values = array();
        /** @var FormsElements $element */
        foreach ($this->_formElements as $key => $element) {
            $error = $element->getErrors();
            if($element != '') {
                $values[$key] = $error;
            }
        }

        return $values;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_formParams;
    }

    /**
     * @param $elementTitle
     * @return bool
     */
    public function isElement($elementTitle)
    {
        return isset($this->_formElements[$elementTitle]);
    }


    /**
     * @param $elementTitle
     * @throws \Exception
     * @return FormsElements
     */
    public function getElement($elementTitle)
    {
        /** @var FormsElements $element */
        if(isset($this->_formElements[$elementTitle])) {
            return $this->_formElements[$elementTitle];
        } else {
            throw new \Exception('Элемент "' . $elementTitle . '" не найден');
        }
    }

    /**
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->_model = $model;
        return $this;
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * @param array $data
     * @param int $id
     * @return int|string
     */
    public function save($data = array(), $id = 0)
    {
        if(!$data) {
            $data = $this->getValues();
        }

        $id = $this->_model->save($data, $id);

        foreach($this->_formElements as $element) {
            if(method_exists($element, 'save')) {
                $element->save($id, $this->_model);
            }
        }

        return $id;
    }

}