<?php
/**
 * Created by PhpStorm.
 * User: Yuri Fayko
 * Date: 30/10/13
 * Time: 10:34 
 */
namespace YF;
/**
 * Class FormsElements
 * @package YF
 * Абстрактный класс для всех элементов формы, все классы элемнтов должны быть отнаследованны от этого класса,
 * и реализовать обязательный метод getHtml
 */
abstract class FormsElements
{

    protected $_name = '';
    protected $_element = array('');

    /**
     * @param string $name
     */
    public function __construct($name = '')
    {
        $this->setName($name);
        $this->_element = array(
            'value' => '',
            'label' => '',
            'validators' => array(),
            'required' => false,
            'filters' => array(),
            'errors' => '',
            'error_message' => ''
        );
        return $this;
    }

    /**
     * Добавляет валидатор для выбранного элемента
     * @param \Zend_Validate_Interface $validator
     * @return $this
     */
    public function addValidator(\Zend_Validate_Interface $validator)
    {
        $this->_element['validators'][] = $validator;
        return $this;
    }

    /**
     * Добавляет сразу несколько валидаторов
     * @param array $validators
     * @return $this
     */
    public function addValidators($validators = array())
    {
        foreach($validators as $validator) {
            $this->addValidator($validator);
        }
        return $this;
    }

    /**
     * Добавляет фильтр для элемента
     * @param \Zend_Filter_Interface $filter
     * @return $this
     */
    public function addFilter(\Zend_Filter_Interface $filter)
    {
        $this->_element['filters'][] = $filter;
        return $this;
    }

    /**
     * Добавляет сразу несколько фильтров
     * @param array $filters
     * @return $this
     */
    public function addFilters($filters = array())
    {
        foreach($filters as $filter) {
            $this->addFilter($filter);
        }
        return $this;
    }

    /**
     * Устанавливает имя элемента
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    /**
     * Возвращает имя элемента
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Делает элемент обязательным
     * @param $isRequired
     * @return $this
     */
    public function setRequired($isRequired)
    {
        $this->_element['required'] = (bool)$isRequired;
        return $this;
    }

    /**
     * Возращает true если элемент обязательный
     * @return bool
     */
    public function isRequired()
    {
        return $this->_element['required'];
    }

    /**
     * Устанавливает значение элемента
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->_element['value'] = $value;
        return $this;
    }

    /**
     * Возвращает значение элемента
     * @return mixed
     */
    public function getValue()
    {
        return $this->_element['value'];
    }

    /**
     * Устанавливает лейбл для элемента
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->_element['label'] = $label;
        return $this;
    }

    /**
     * Возвращает лейбл
     * @return string
     */
    public function getLabel()
    {
        return $this->_element['label'];
    }

    /**
     * Устанавливает сообщение об ошибке
     * @param string $message
     * @return $this
     */
    public function setErrorMessage($message)
    {
        $this->_element['error_message'] = $message;
        return $this;
    }

    /**
     * Если произошла ошибка, то выводит сообщение об ошибке
     * @param string $messages
     */
    public function setErrors($messages = '')
    {
        if(is_array($messages)) {
            if($this->_element['error_message']) {
                $this->_element['errors'] = $this->_element['error_message'];
            } else {
                foreach ($messages as $message) {
                    $this->_element['errors'] = $message;
                    break;
                }
            }
        } else {
            $this->_element['errors'] = $messages;
        }

    }

    /**
     * Устанавливает сообщение об обязательном поле
     * @param $message
     * @return $this
     */
    public function setRequiredMessage($message)
    {
        $this->_element['required_error_message'] = $message;
        return $this;
    }

    /**
     * Возвращает сообщение об обязательном поле
     * @return string
     */
    public function getRequiredMessage()
    {
        if(isset($this->_element['required_error_message'])) {
            return $this->_element['required_error_message'];
        } else {
            return 'Поле не должно быть пустым';
        }
    }

    /**
     * Возвращает все текущие ошибки
     * @return mixed
     */
    public function getErrors()
    {
        return $this->_element['errors'];
    }

    /**
     * Фильтрует элемент установленными ранее фильтрами
     */
    public function filter()
    {
        $value = $this->getValue();

        /** @var $filter \Zend_Filter_Interface */
        foreach($this->_element['filters'] as $filter) {
            $value = $filter->filter($value);
        }

        $this->setValue($value);
    }

    /**
     * Проверяет элемент установленными ранее валидаторами
     * @return bool
     */
    public function isValid()
    {
        $value = $this->getValue();
        $valid = true;
        /** @var $validator \Zend_Validate_Abstract */
        foreach($this->_element['validators'] as $validator) {
            if(!$validator->isValid($value)) {
                $valid = false;
                $this->setErrors($validator->getMessages());
            }
        }
        return $valid;
    }

    /**
     * Возвращает id элемента
     * @param Forms $form
     * @return string
     */
    protected function getElementId(Forms $form)
    {
        return $this->getName() . '-' . $form->getName();
    }

    /**
     * Возвращает класс элемента
     * @param string $class
     * @return string
     */
    protected function getElementClass($class = '')
    {
        if($this->isRequired()) {
            $class .= ' required';
        }

        $class = trim($class);

        return $class;
    }


    /**
     * Этот метод должен быть реализован в классе элемента
     * @param Forms $form
     * @param string $class
     * @return mixed
     */
    abstract public function getHtml(Forms $form, $class = '');
}