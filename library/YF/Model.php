<?php
namespace YF;
/**
 * Class Model
 * @package YF
 */
class Model
{
    const IMAGE_ORIGINAL = 0;
    const IMAGE_CROP     = 1;
    const IMAGE_RESIZE   = 2;

    const RELATION_ONE_TO_ONE = 0;
    const RELATION_ONE_TO_MANY = 1;
    const RELATION_ONE_TO_ONE_PARENT = 2;

    protected $_tableName     = 'items';
    protected $_serviceFields = array('is_del');

    protected $_relation = array();

    protected $_requireFields = array();
    protected $_noSaveFields  = array();
    protected $_galleryFields = array();

    protected $_userId = 0;

    protected $_currentId = 0;
    protected $_currentSaveData = array();

    protected $_createdDateField  = 'created_date';
    protected $_modifiedDateField = 'modified_date';
    protected $_createdUserField  = 'user_id';
    protected $_modifiedUserField = 'modified_user_id';

//    protected $_galleryFields = array('images' =>
//    array(
//        array(
//            'type' => self::IMAGE_CROP,
//            'landscape' => array('width' => 200, 'height' => 200),
//            'portrait' => array('width' => 200, 'height' => 200)
//        ),
//        array(
//            'type' => self::IMAGE_CROP,
//            'landscape' => array('width' => 100, 'height' => 100),
//            'portrait' => array('width' => 100, 'height' => 100)
//        )
//    )
//    );


    /*
     * Пример
     * protected $_imageFields  = array();
     *  'file' => array(
     *       array('width' => 100, 'height' => 100, 'type' => self::IMAGE_CROP),
     *       array('width' => 200, 'height' => 0,   'type' => self::IMAGE_RESIZE),
     *       array('type' => self::IMAGE_ORIGINAL)
     *   )
     *
     */
    protected $_imageFields  = array();


    protected $_fileFields = array();

    protected $_imageSavePath; // if empty imageSavePath will be 'files/{tableName}/'

    protected $_sortFields = array('order');

    protected $_limitCount = 0;
    protected $_order = 'id DESC';
    protected $_where = 'is_del = 0';

    protected $_whereForUpdate = null;

    /** @var \Zend_Db_Adapter_Pdo_Mysql */
    protected $_db;
    protected $_cache;
    protected $_auth;
    protected $_acl;
    protected $_config;
    protected $_site;

    protected $_numRows = null;

    /**
     * @param string $tableName
     */
    public function __construct($tableName = '')
    {
        if($tableName) {
            $this->setTableName($tableName);
        }

        $this->_db = \Zend_Registry::get('dbAdapter');

        if(is_null($this->_imageSavePath)) {
            $this->_imageSavePath  = 'files/' . $this->_tableName . '/';
        }

    }

    /**
     * @param string $tableName
     * @return $this
     */
    public function setTableName($tableName)
    {
        $this->_tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->_tableName;
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUser($userId)
    {
        $this->_userId = (int)$userId;
        return $this;
    }

    /**
     * @param $where
     * @return $this
     */
    public function setWhereForUpdate($where)
    {
        $this->_whereForUpdate = $where;
        return $this;
    }

    /**
     * @param $name
     * @param string $key
     * @param int $type
     * @param Model $model
     */
    public function addRelation($name, $key = 'id', $type = self::RELATION_ONE_TO_ONE, Model $model = null)
    {
        if(is_null($model)) {
            $model = new Model($name);
        }

        $this->_relation[$name] = array('key' => $key, 'model' => $model, 'type' => $type);
    }


    /**
     * @param string $name
     * @param array $dbData
     * @return array|bool
     */
    protected function getRelationData($name, $dbData)
    {
        if(!isset($this->_relation[$name])) {
            return array();
        }

        $relation = $this->_relation[$name];
        /** @var Model $model */
        $model = $relation['model'];

        $data = array();

        if($relation['type'] === self::RELATION_ONE_TO_ONE) {
            $data = $model->fetchRow($dbData[$relation['key']]);
        } else if($relation['type'] === self::RELATION_ONE_TO_MANY) {
            $data = $model->fetchAll($relation['key'] . '=' . $dbData['id']);
        } else if($relation['type'] === self::RELATION_ONE_TO_ONE_PARENT) {
            $data = $model->fetchRow($relation['key'] . '=' . $dbData['id']);
        }

        return $data;
    }

    /**
     * @param string|array $where
     * @param string $order
     * @param int $limit
     * @param int $page
     * @return array
     */
    public function fetchAll($where = '', $order = '', $limit = 0, $page = 0)
    {
        $select = $this->_db->select()->from($this->_tableName, new \Zend_Db_Expr('SQL_CALC_FOUND_ROWS *'));
        if($where) {
            $select->where($where);
        }

        $select->where($this->_where);

        if($order) {
            $select->order($order);
        }

        $select->order($this->_order);

        if($limit > 0) {
            $offset = $page * $limit;
            $select->limit($limit, $offset);
        }

        $items = $select->query()->fetchAll();
        $this->_numRows = (int)$this->_db->fetchOne("SELECT FOUND_ROWS()");

        foreach($items as &$item) {
            foreach($this->_relation as $relationField => $relation) {
                $item[$relationField] = $this->getRelationData($relationField, $item);
            }

            $imagesPaths = array();
            foreach($this->_imageFields as $imageName => $params) {
                $imagesPaths = $this->getImageFullPath($imageName, $item);
            }

            $item = array_merge($item, $imagesPaths);

            $item = $this->additionalFields($item);
        }

        return $items;
    }

    /**
     * @return null
     */
    public function getRowsCount()
    {
        return $this->_numRows;
    }

    /**
     * @param string|int|array $where
     * @return bool|array
     */
    public function fetchRow($where = '')
    {
        $select = $this->_db->select()->from($this->_tableName);

        if(!is_array($where) and (int)$where > 0) {
            $select->where('id = ' . (int)$where);
        } else if(is_array($where)){
            foreach($where as $w) {
                $select->where($w);
            }
        } else if($where != '') {
            $select->where($where);
        }

        $select->where( $this->_where);

        $item = $select->query()->fetch();

        if(!$item) return false;

        foreach($this->_relation as $relationField => $relation) {
            $item[$relationField] = $this->getRelationData($relationField, $item);
        }

        $imagesPaths = array();
        foreach($this->_imageFields as $imageName => $params) {
            $imagesPaths = $this->getImageFullPath($imageName, $item);
        }

        $item = array_merge($item, $imagesPaths);
        $item = $this->additionalFields($item);

        return $item;
    }

    /**
     * @param string $where
     * @param string|array $field
     * @param string $order
     * @return array
     */
    public function fetchPairs($where = '', $field = 'title', $order = '')
    {
        if(is_array($field)) {
            $fields = implode(', ', $field);
            $field = 'CONCAT(' . $fields . ')';
        }

        $select = $this->_db->select()->from($this->_tableName, array('id', $field));

        if($where) {
            $select->where($where);
        }
        $select->where($this->_where);

        if($order) {
            $select->order($order);
        }

        $select->order($this->_order);


        $items = $this->_db->fetchPairs($select);
        return $items;

    }

    /**
     * @param $data
     * @param string $where
     * @return int|string
     */
    public function save($data, $where = '')
    {
        $this->_db->beginTransaction();
        $id = 0;

        if(isset($data['id'])) {
            $id = (int)$data['id'];
            unset($data['id']);
        }

        $updateWhere[] = 'id = ' . $id;
        if(is_array($where)) {
            foreach($where as $w) {
              $updateWhere[] = $w;
            }
        } else if($where != '') {
            $updateWhere[] = $where;
        }


        $schemeDb = $this->_db->describeTable($this->_tableName);
        $saveData = array();
        foreach($data as $field => $value) {
            if(isset($schemeDb[$field])) {
               $saveData[$field] = $value;
            }
        }


        if($id > 0) {

            if(isset($schemeDb[$this->_modifiedDateField])) {
                $saveData[$this->_modifiedDateField] = time();
            }

            if(isset($schemeDb[$this->_modifiedUserField]) and $this->_userId > 0) {
                $saveData[$this->_modifiedUserField] = $this->_userId;
            }

            if(!is_null($this->_whereForUpdate)) {
                $updateWhere[] = $this->_whereForUpdate;
            }
            $this->_db->update($this->_tableName, $saveData, $updateWhere);
        } else {

            if(isset($schemeDb[$this->_createdDateField])) {
                $saveData[$this->_createdDateField] = time();
            }

            if(isset($schemeDb[$this->_createdUserField]) and $this->_userId > 0) {
                $saveData[$this->_createdUserField] = $this->_userId;
            }

            $this->_db->insert($this->_tableName, $saveData);
            $id = $this->_db->lastInsertId();
        }

        $this->_currentId = $id;
        $this->_currentSaveData = $saveData;

        $this->_db->commit();

        return $id;
    }

    /**
     * @param array $data
     * @param int $id
     */
    public function update($data, $id)
    {
        $updateWhere = array('id = ' . (int)$id);

        if(!is_null($this->_whereForUpdate)) {
            $updateWhere[] = $this->_whereForUpdate;
        }
        $this->_db->update($this->_tableName, $data, $updateWhere);
    }

    /**
     * @param $order
     */
    public function saveSort($order)
    {
        foreach($order as $field => $sort) {

            if(!in_array($field, $this->_sortFields)) continue;

            $i = 0;
            foreach($sort as $id) {
                $id = (int)$id;
                if($id == 0) break;
                $this->_db->update($this->_tableName, array($field => $i), '`id` = ' . $id);
                $i++;

            }
        }
    }

    /**
     * @param int $id
     */
    public function remove($id)
    {
        $this->_db->update($this->_tableName, array('is_del' => 1), '`id` = ' . (int)$id);
    }

    /**
     * @param int $id
     */
    public function restore($id)
    {
        $this->save(array('is_del' => 0), $id);
    }

    /**
     * @param $id
     */
    public function removeFromGallery($id)
    {
        $this->_db->update('gallery', array('is_del' => 1), '`id` = ' . (int)$id);
    }

    /**
     * @param $table
     * @param $data
     * @param array $settings
     */
    public function multipleInsert($table, $data, $settings = array())
    {
        $keys = array_keys($data[0]);
        foreach($keys as &$key) {
            $key = '`' . $key . '`';
        }
        $keysStr = implode(', ', $keys);

        if(isset($settings['ignore_duplicate']) and $settings['ignore_duplicate']) {
            $ignore = 'IGNORE';
        } else {
            $ignore = '';
        }

        $sql = "INSERT " . $ignore . " INTO `" . $table . "` (" . $keysStr . ") VALUES ";
        $i = 0;
        foreach($data as $row) {
            if($i > 0) $sql .= ', ';
            foreach($row as $key => &$value) {
                $value = $this->_db->quote($value);
            }
            $rowStr = implode($row, ', ');
            $sql .= "(" . $rowStr . ")";
            $i++;
        }

        $this->_db->getConnection()->exec($sql);
    }

    /**
     * Increment field or fields on offset value
     * @param int $id
     * @param string|array $fields
     * @param int $offset
     */
    public function incFields($id, $fields, $offset = 1)
    {
        if(!is_array($fields)) {
            $fields = array($fields);
        }
        $incData = array();
        foreach ($fields as $field) {
            $incData[$field] = new \Zend_Db_Expr($this->_db->quoteIdentifier($field) . ' + ' . $offset);
        }

        $this->_db->update($this->_tableName, $incData, $this->_db->quoteInto("`id` = ?", $id));
    }

    /**
     * @return int
     */
    public function getCurrentId()
    {
        return $this->_currentId;
    }

    /**
     * @return array
     */
    public function getCurrentSaveData()
    {
        return $this->_currentSaveData;
    }

    /**
     * @return string
     */
    public function getImageSavePath()
    {
        return $this->_imageSavePath . $this->getCurrentId() . '/';
    }

    /**
     * @param $imageName
     * @return array|bool
     */
    public function getImageParams($imageName)
    {
        if(isset($this->_imageFields[$imageName])) {
            /** @var array $params */
            $params = $this->_imageFields[$imageName];
            return $params;
        } else {
            return false;
        }
    }

    /**
     * @param $imageName
     * @param $dbData
     * @return array|string
     */
    protected function getImageFullPath($imageName, $dbData)
    {
        if(!isset($this->_imageFields[$imageName])) {
            return '';
        }

        $return = array();
        foreach($this->_imageFields[$imageName] as $params) {

            switch($params['type']) {
                case Model::IMAGE_CROP:
                case Model::IMAGE_RESIZE:
                    $return[$imageName . '_' . $params['width'] . '_' . $params['height']] = $this->getImageSavePath() . $params['width'] . '_' . $params['height'] . '_' . $dbData[$imageName];
                    break;
                case Model::IMAGE_ORIGINAL:
                    $return[$imageName . '_original'] = $this->getImageSavePath() . $dbData[$imageName];
                    break;
            }

        }

        return $return;
    }

    /**
     * Заглушка для добавления дополнительных полей в моделях
     * @param $dbData
     * @return mixed
     */
    protected function additionalFields($dbData)
    {
        return $dbData;
    }

}